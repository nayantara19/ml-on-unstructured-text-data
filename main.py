import numpy as np
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
import scipy
import os
from sklearn.feature_selection import VarianceThreshold
from scipy import sparse
from scipy.io import mminfo,mmread,mmwrite
from sklearn.feature_selection import SelectKBest
from sklearn.feature_selection import chi2
from datetime import datetime
from sys import argv

os.chdir('C:\Python27\learning_from_unstructured_data')

def call_countvectorizer(data):
    count_vect = CountVectorizer()
    count_vec_matrix = count_vect.fit_transform(data)
    #print count_vect.vocabulary
    return count_vec_matrix

def call_tfidftransformer(data):
    tf_transformer = TfidfTransformer(use_idf=True).fit(data)
    tfidf_matrix = tf_transformer.transform(data)
    return tfidf_matrix

def transform_countvec_matrix(mat):
    #convert read matrix to a csr matrix
    mat = mat.tocsr()
    #transpose matrix
    mat =  mat.transpose()
    return mat

def convert_mtx_file_to_csv(mtx_data):
    mat = mmread(mtx_data)
    return mat.toarray()

def convert_csv_to_mtx(csv_data):


def read_mtx_file(filename,type_of_matrix):


def read_csv_file(filename):


def labels(specify_format):
    '''
    'This function is used to read classes file and return an array of labels for all 2225 documents'
    '''
    labels_file = open('original_dataset/bbc.classes','r+')
    labels_content=labels_file.read()
    #labels_array = labels_content.toarray()
    temp_list=[]
    for a in labels_content.splitlines():
        temp_list.append(a.split(' '))
    temp_list=np.asarray(temp_list)
    temp_list=temp_list[4:]
    labels_array = [item[1] for item in temp_list]
    if specify_format=="row":
        return labels_array
    else:
        return zip(*labels_array)

def variance_threshhold(tfidf_matrix):
    '''
        (tfidf matrix) -> CSV file in 'output_dataset' folder with low variance features removed
        'Removes features with variance less than 0.000017669'
    '''
    tfidf_numpy = tfidf_matrix.toarray()
    selector = VarianceThreshold(threshold=0.000017669)
    tfidf_numpy_variance_threshold = selector.fit_transform(tfidf_numpy)
    np.savetxt("output_dataset/test.csv",tfidf_numpy_variance_threshold , delimiter=",")

def select_k_best_features(tfidf_matrix):
    '''
    (tfidf matrix) -> CSV file in 'output_dataset' folder with best 5000 features
    'Removes features with high correlation'
    '''

    labels=labels('row')
    tfidf_numpy = tfidf_matrix.toarray()

    '''
    for i in range(tfidf_numpy.shape[0]):
        for j in range(tfidf_numpy.shape[1]):
            if(tfidf_numpy[i][j]!=0):
                tfidf_numpy[i][j]=1
            else:
                tfidf_numpy[i][j]=0
    '''
    tfidf_numpy[tfidf_numpy>0]=1
    tfidf_numpy[tfidf_numpy==0]=0
    tfidf_features_removed = SelectKBest(chi2, k=5000).fit_transform(tfidf_numpy, labels)
    print tfidf_features_removed.shape
    return tfidf_features_removed

if __name__ == '__main__':

    # Read BBC count vectorizer matrix from .mtx file and transform it
    mat = mmread('original_dataset/bbc.mtx')
    mat = transform_countvec_matrix(mat)

    # Generate tfidf matrix from the above count vecorizer matrix
    tfidf_matrix= call_tfidftransformer(mat)

    # Write the final tfidf matrix to a .mtx file
    mmwrite('output_dataset/tfidfweightsmatrix.mtx',tfidf_matrix)

    # Call variance threshhold function to remove features with low tfidf variance
    variance_threshhold(tfidf_matrix)

    # Use selectkbest features function
    # a=datetime.now()
    # Function took 55 seconds to execute when each item in numpy matrix was iterated through
    # Whereas it took only 1 second to execute when tfidf_numpy[condition]=x was used
    # select_k_best_features(t_data)
    # print (datetime.now()-a)

    #print labels('')
    print labels('row')
